//
//  Constants.swift
//  Kiro-Radio
//
//  Created by Steele Nelson on 8/13/15.
//  Copyright (c) 2015 Bonneville. All rights reserved.
//

import Foundation



//MARK: Core Data
let kShowEntityName: String = "Show"
let kShowAudioEntityName: String = "ShowAudio"
let kHighlightEntityName: String = "Highlight"
let kPodAudioEntityName: String = "PodAudio"
let kHighlightGroupEntityName: String = "HighlightGroup"
let kStationEntityName: String = "Station"
let kScheduleEntityName: String = "Schedule"
let kDayEntityName: String = "Day"
let kTimeslotEntityName: String = "TimeSlot"
let kBlackoutGameEntityName: String = "BlackoutGame"
let kNewsItemEntityName: String = "NewsItem"
let kDataModelName: String = "DataModel"
let kClipEntityName: String = "Clip"
let kContestEntityName: String = "Contest"
let kCalendarEventEntityName: String = "CalendarEvent"
let kCueEntityName: String = "Cue"
let kTrackEntityName: String = "Track"
let kLatestItemEntityName: String = "Latest"

//MARK: RestKit KeyPaths
let kShowKeyPath: String = "station.shows.show"
let kShowAudioKeyPath: String = "ondemand_show.files.file"
let kEpisodeShowAudioKeyPath: String = "ondemand_episode.file.file"
let kHighlightGroupKeyPath: String = "highlights.pubdate"
let kHighlightGroupAudioBoomKeyPath: String = "rss.channel"
let kStationKeyPath: String = "station"
let kBlackoutGameKeyPath: String = "blackouts.game"
let kNewsItemKeyPath: String = "rss.channel.item"
let kNewsItemAudioBoomKeyPath: String = "rss.channel.item"
let kClipAudioBoomKeyPath: String = "rss.channel.item"
let kContestItemWordpressKeyPath: String = "rss.channel.item"
let kCalendarEventItemWordpressKeyPath: String = "rss.channel.item"
let kCueRecentlyPlayedKeyPath: String = ""
let kTrackRecentlyPlayedKeyPath: String = "data"
let kLatestItemWordpressKeyPath: String = "rss.channel.item"



//MARK: RestKit XML Maps
let kShowXMLMapName: String = "show_xml_map"
let kShowAudioXMLMapName: String = "show_audio_xml_map"
let kHighlightXMLMapName: String = "highlight_xml_map"
let kHighlightAudioBoomXMLMapName: String = "audioboom_highlights_xml_map"
let kPodAudioXMLMapName: String = "pod_audio_xml_map"
let kPodAudioAudioBoomXMLMapName: String = "audioboom_pod_audio_xml_map"
let kHighlightGroupXMLMapName: String = "highlight_group_xml_map"
let kHighlightGroupAudioBoomXMLMapName: String = "audioboom_highlight_group_xml_map"
let kTimeslotXMLMapName: String = "timeslot_xml_map"
let kDayXMLMapName: String = "day_xml_map"
let kScheduleXMLMapName: String = "schedule_xml_map"
let kStationXMLMapName: String = "station_xml_map"
let kBlackoutGameXMLMapName: String = "blackout_game_xml_map"
let kNewsItemXMLMapName: String = "news_item_xml_map"
let kNewsItemAudioBoomXMLMapName: String = "audioboom_newsaudio_xml_map"
let kClipAudioBoomXMLMapName: String = "audioboom_clip_xml_map"
let kContestWordpressXMLMapName: String = "contests_xml_map"
let kCalendarEventWordpressXMLMapName: String = "calendar_event_xml_map"
let kCueJSONMapName: String = "music_cue_json_map"
let kTrackJSONMapName: String = "music_track_json_map"
let kLatestItemWordpressXMLMapName : String = "latest_xml_map"

//MARK: Image Dimensions
let kShowImageDimensions: CGSize = CGSize(width: 900, height: 600)

//MARK: Fetch Limits
let kOnDemandHighlightsLimit = 12
let kRadioHighlightsLimit = 10
let kRadioShowsLimit = 10

//MARK: OnDemand Section Labels
let kOnDemandShowsLabel = "SHOWS"
let kOnDemandHighlightsLabel = "HIGHLIGHTS"


//MARK: Notifications
let kReceivedShows = "com.kiroradio.ReceivedShows"
let kReceivedSchedule = "com.kiroradio.ReceivedSchedule"
let kCurrentTimeSlotChanged = "com.kiroradio.CurrentTimeSlotChanged"
let kTappedPlaybar = "com.kiroradio.TappedPlaybar"
let kTappedPlaybarPlayPause = "com.kiroradio.TappedPlaybarPlayPause"


//MARK: Audio
let kKIROMount = "KIROFMAAC"

//MARK: Triton
let kTritonPlayerServicesDomain = "playerservices.streamtheworld.com"
let kTritonLiveStreamPath = "/api/livestream"
let kTritonAPIVersion = "1.8"
let kTritonMountpointKeyPath = "mountpoints.mountpoint"
let kTritonMountKeyPath = "mountpoints.mountpoint.mount"
let kTritonMountServerPath = "mountpoints.mountpoint.servers.server"
let kTritonIPKeyPath = "ip"
let kTritonPortsKeyPath = "ports.port"
let kTritonStatusCodePath = "status.status-code"


//MARK: Blackout
let kBlackoutAlertTitle = "Live Stream Blackout"
let kBlackoutAlertMessage = {(home: String, visitor: String, endTime: String) -> String in
    return "Due to contractual agreements, the live audio stream during the \(visitor) at \(home) game is only available to listeners in the \(kBlackoutArea) area.  The stream will resume at \(endTime) for those outside the \(kBlackoutArea) area."
}

//MARK: Preferences
let kOnDemandSectionKey = "onDemandSectionKey"
let kUpdatesSectionKey = "updatesSectionKey"

//MARK: NSUserActivity

let kBrowseShowAudioActivityType = "com.bonneville.kiroam.browseShowAudio"



//MARK: Playback Errors
let kPlaybackErrorTitle = "Playback Error"

//MARK: Privacy
let kPrivacyStatement = "More about Nielsen"


//MARK: User Preference Keys
let kVideoBackgroundedKey = "com.bonneville.preferences.videoBackgrounedKey"


//MARK: Navigation Spacing
let kPoweredByLogoSpacing : CGFloat = 8

#if DEBUG
let kNielsenDebug = "true"
#else
let kNielsenDebug = "false"
#endif


//MARK: did show location registration key
let kDidShowLocationRegistration = "com.bonneville.preferences.didShowLocationRegistration"

//MARK: Soft Ask Notifications
let kSessionsSoFarKey = "SESSIONS_SO_FAR"
let kShownHardAskKey = "SHOWN_HARD_ASK"

//MARK: Event notifications
let kAudioManagerStartedPlaying = "com.bonneville.preferences.AudioManager.StartedPlaying"
let kAudioManagerStoppedPlayign = "com.bonneville.preferences.AudioManager.StoppedPlaying"

let kContestRulesLabel = "Contest Rules"

let kLocationPListDescriptionKey = "NSLocationWhenInUseUsageDescription"
let kStatusCellIdentifier = "status_cell"

//MARK: More Tab
let kDisabledLocationColor = UIColor.red
let kEnabledLocationColor = UIColor.green
let kDisabledPushNotificationColor = UIColor.red
let kEnabledPushNotificationColor = UIColor.green



