//
//  Ex_UIViewController.swift
//  radio_ui_ios_framework
//
//  Created by Cody Nelson on 11/9/17.
//  Copyright © 2017 Bonneville. All rights reserved.
//

import Foundation

extension UIViewController {
    func showInternetStatusView(){
        let alert = UIAlertController(title: "No Internet Connection", message: "Try exiting airplane mode or connecting to Wifi.", preferredStyle: .actionSheet)
        self.present(alert, animated: true, completion: nil)
    }
}
