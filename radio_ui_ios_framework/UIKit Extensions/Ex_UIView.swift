//
//  Ex_UIView.swift
//  radio_ui_ios_framework
//
//  Created by Cody Nelson on 11/9/17.
//  Copyright © 2017 Bonneville. All rights reserved.
//

import Foundation
import Foundation

import UIKit

extension UIView {
    func constraintForAttrToView(_ view: UIView, attr: NSLayoutAttribute, item: AnyObject!) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: view, attribute: attr, relatedBy: NSLayoutRelation.equal, toItem: item, attribute: attr, multiplier: 1.0, constant: 0);
    }
    func constraintForAttrToSelf(_ attr: NSLayoutAttribute, item: AnyObject!) -> NSLayoutConstraint {
        return constraintForAttrToView(self, attr: attr, item: item)
    }
}


