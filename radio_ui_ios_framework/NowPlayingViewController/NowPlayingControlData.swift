//
//  NowPlayingControlData.swift
//  radio_ui_ios_framework
//
//  Created by Cody Nelson on 11/9/17.
//  Copyright © 2017 Bonneville. All rights reserved.
//

import Foundation

/// NowPlayingControlData
///
/// This is a data object that can be stored in the Persistence Stack.  From there, it can be referenced by the
/// NowPlayingControl.swift, which will extract the data fields and apply them to the control.  If there value is left empty,
/// a default value can be set.
public struct NowPlayingControlData{
    
    var host : String = "KOIT San Francisco"
    
    var description : String = "Song Title Here"
    
    var buttonTitle : String = "Watch Now"
    
    var image : UIImage = UIImage(named: "dannydaveandmoore", in: Bundle.init(for: NowPlayingControl.self) , compatibleWith: nil)!
    
    var labelColor : CGColor = UIColor.white.cgColor
    
    var labelTransparency : CGFloat = 0.8
}
