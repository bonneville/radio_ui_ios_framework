//
//  NowPlayingContol.swift
//  radio_ui_ios_framework
//
//  Created by Cody Nelson on 11/8/17.
//  Copyright © 2017 Bonneville. All rights reserved.
//

import UIKit

public class NowPlayingControlInternal : UIView {
    @IBOutlet
    var imageView : UIImageView?
    @IBOutlet
    var hostLabel : UILabel?
    @IBOutlet
    var descriptionLabel : UILabel?
    
    @IBOutlet
    var button : UIButton?
    
    @IBOutlet
    var clickableContainer : UIView?
}
@IBDesignable
public class NowPlayingControl: UIControl {
    
    var _internal : NowPlayingControlInternal?
    
//    override public func awakeFromNib() {
//        super.awakeFromNib()
//        _ = self.imageView
//        _ = self.button
//        _ = self.hostLabel
//        _ = self.descriptionLabel
//    }
    
    
    
    enum NowPlayingContolError {
        
    }
    
    @IBInspectable
    var image : UIImage? = UIImage(named: "dannydaveandmoore" ){
        didSet{
            if let tmpInternal = self._internal {
                tmpInternal.imageView?.image = image!
            }
        }
    }
    
    @IBInspectable
    var hostName : String? = "" {
        didSet{
            if let tmpInternal = self._internal {
                tmpInternal.hostLabel?.text = hostName
            }
        }
    }
    
    @IBInspectable
    public var descriptionText : String? = "" {
        didSet{
            if let tmpInternal = self._internal {
                tmpInternal.descriptionLabel?.text = descriptionText
            }
        }
    }
    
    @IBInspectable
    var buttonTitle : String? = "" {
        didSet{
            if let tmpInternal = self._internal {
                tmpInternal.button?.setTitle(buttonTitle, for: .normal)
            }
        }
    }
    
    @IBInspectable
    var labelColor : CGColor? = UIColor.white.cgColor {
        didSet{
            if let tmpInternal = self._internal {
                tmpInternal.descriptionLabel?.textColor = UIColor(cgColor: labelColor!)
                tmpInternal.hostLabel?.textColor = UIColor(cgColor: labelColor!)
                tmpInternal.button?.setTitleColor(UIColor(cgColor: labelColor!) , for: .normal)
            }
        }
    }
    
    @IBInspectable
    var labelContainerAlpha : CGFloat = 0.80{
        didSet{
            if let tmpInternal = self._internal {
                tmpInternal.clickableContainer?.alpha = labelContainerAlpha
            }
        }
    }
    
    
    
    func commonInit() {
        if(self._internal != nil) {
            return
        }
        
        self.isUserInteractionEnabled = false
        let bundle = Bundle(for: NowPlayingControl.self)
        
        
        let nib = UINib(nibName: "NowPlayingControl", bundle: bundle)
        if let internalRoot = nib.instantiate(withOwner: self, options: nil)[0] as? UIView {
            if let _internal = internalRoot.subviews[0] as? NowPlayingControlInternal {
                self._internal = _internal
                
                addSubview(_internal)
                self.addConstraints([ constraintForAttrToSelf(NSLayoutAttribute.width, item: _internal),
                                      constraintForAttrToSelf(NSLayoutAttribute.height, item: _internal),
                                      constraintForAttrToSelf(NSLayoutAttribute.top, item: _internal),
                                      constraintForAttrToSelf(NSLayoutAttribute.left, item: _internal)])
            }
        }
        self.backgroundColor = UIColor.white
    }
    
    
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
 
    
    
  
    
    


    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
