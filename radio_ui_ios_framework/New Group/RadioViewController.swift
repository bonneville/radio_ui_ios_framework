//
//  RadioViewController.swift
//  radio_ui_ios_framework
//
//  Created by Cody Nelson on 11/8/17.
//  Copyright © 2017 Bonneville. All rights reserved.
//

import UIKit

public class RadioViewController: UIViewController {
    
    enum RadioViewControllerError{
        case PersistentStackNotFound
        
        func description()->String{
            switch(self){
            case .PersistentStackNotFound:
                return "Persistent Stack Not Found!"
            }
        }
    }
    
    var persistenceStack : PersistenceStack?
    
    @IBOutlet
    weak var tableView : UITableView?
    
    
    @IBOutlet
    weak var nowPlayingControl : NowPlayingControl? {
        didSet{
            
        }
    }
    
    func configNowPlayingControl( configData : NowPlayingControlData ){
//        self.nowPlayingControl = NowPlayingControl(initWith: configData.image
//            , hostName: configData.host, description: configData.description, buttonTitle: configData.buttonTitle)
        self.nowPlayingControl?.image = configData.image
        self.nowPlayingControl?.buttonTitle = configData.buttonTitle
        self.nowPlayingControl?.hostName = configData.host
        self.nowPlayingControl?.descriptionText = configData.description
        self.nowPlayingControl?.labelColor = configData.labelColor
        self.nowPlayingControl?.labelContainerAlpha = configData.labelTransparency
        print("Configured NowPlayingControl")
    }
    
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let persistenceStack = persistenceStack else { fatalError( RadioViewControllerError.PersistentStackNotFound.description() )}
        
        configNowPlayingControl(configData: persistenceStack.nowPlayingControlData)
        configNavBar( configData: persistenceStack.radioViewControllerData )
        
        

        // Do any additional setup after loading the view.
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    public override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.nowPlayingControl?.layoutIfNeeded()
    }
    
    func configNavBar( configData : RadioViewControllerData){
        //TODO: change this when you leave.
        let navBar = self.navigationController?.navigationBar
        configData.removeNavBar ? (navBar?.isHidden = true) : (navBar?.isHidden = false)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
