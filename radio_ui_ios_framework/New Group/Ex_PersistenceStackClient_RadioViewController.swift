//
//  Ex_PersistenceStackClient_RadioViewController.swift
//  radio_ui_ios_framework
//
//  Created by Cody Nelson on 11/8/17.
//  Copyright © 2017 Bonneville. All rights reserved.
//

import Foundation
extension RadioViewController : PersistenceStackClient {
    public func setStack(stack: PersistenceStack) {
        self.persistenceStack = stack
    }
}
