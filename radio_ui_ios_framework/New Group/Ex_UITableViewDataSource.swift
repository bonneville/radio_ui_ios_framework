//
//  Ex_UITableViewDataSource_RadioViewController.swift
//  radio_ui_ios_framework
//
//  Created by Cody Nelson on 11/9/17.
//  Copyright © 2017 Bonneville. All rights reserved.
//

import Foundation

extension RadioViewController : UITableViewDataSource{
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
}
